//
// COBSPacket.cpp 
// Class library C++ code
// ----------------------------------
// Developed with embedXcode+ 
// http://embedXcode.weebly.com
//
// Project 		COBSPacketTest
//
// Created by 	Andy, 6/4/15 7:14 AM
// 				StuffAndyMakes.com
//
// Copyright 	(c) Andy, 2015
// Licence		<#license#>
//
// See 			COBSPacket.h and ReadMe.txt for references
//


// Library header
#include "COBSPacket.h"


/**
 * \file crc16.c
 * Table & function for 16-bit CRC checks.
 * (Unnecessary functions, inlines, etc. removed)
 *
 * Generated on Thu Jun  4 12:06:22 2015,
 * by pycrc v0.8, http://www.tty1.net/pycrc/
 * using the configuration:
 *    Width        = 16
 *    Poly         = 0x8005
 *    XorIn        = 0x0000
 *    ReflectIn    = True
 *    XorOut       = 0x0000
 *    ReflectOut   = True
 *    Algorithm    = table-driven
 *****************************************************************************/

/**
 * Static table used for the table_driven implementation.
 *****************************************************************************/
static const uint16_t crc_table[16] = {
    0x0000, 0xcc01, 0xd801, 0x1400, 0xf001, 0x3c00, 0x2800, 0xe401,
    0xa001, 0x6c00, 0x7800, 0xb401, 0x5000, 0x9c01, 0x8801, 0x4400
};

/**
 * Update the crc value with new data.
 *
 * \param crc      The current crc value.
 * \param data     Pointer to a buffer of \a data_len bytes.
 * \param data_len Number of bytes in the \a data buffer.
 * \return         The updated crc value.
 *****************************************************************************/
uint16_t COBSPacket::_crc16(const uint8_t *data, size_t data_len) {
    uint16_t crc = 0x0000;
    uint16_t tbl_idx;
    
    while (data_len--) {
        tbl_idx = crc ^ (*data >> (0 * 4));
        crc = crc_table[tbl_idx & 0x0f] ^ (crc >> 4);
        tbl_idx = crc ^ (*data >> (1 * 4));
        crc = crc_table[tbl_idx & 0x0f] ^ (crc >> 4);
        
        data++;
    }
    // af: added finalize() to this return
    return ((crc & 0xffff) ^ 0x0000);
}

/*
 * [_stuff] byte stuffs "length" bytes of
 * data at the location pointed to by "ptr",
 * writing the output to the location pointed
 * to by "dst".
 *** From http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
 */
//#define FinishBlock(X) (*code_ptr = (X), code_ptr = dst++, code = 0x01)
//void COBSPacket::_stuff(const uint8_t *ptr, uint8_t length, uint8_t *dst) {
//    const uint8_t *end = ptr + length;
//    uint8_t *code_ptr = dst++;
//    uint8_t code = 0x01;
//    while (ptr < end) {
//        Serial.print(*dst, HEX);
//        Serial.print(" ");
//        if (*ptr == 0) {
//            FinishBlock(code);
//        } else {
//            *dst++ = *ptr;
//            code++;
//            if (code == 0xFF) {
//                FinishBlock(code);
//            }
//        }
//        ptr++;
//    }
//    FinishBlock(code);
//}


/*
 * StuffData byte stuffs "length" bytes of
 * data at the location pointed to by "ptr",
 * writing the output to the location pointed
 * to by "dst".
 *** From http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
 */
#define FinishBlock(X) (*code_ptr = (X), code_ptr = dst++, code = 0x01)
void COBSPacket::_stuff(const uint8_t *ptr, uint8_t length, uint8_t *dst) {
    const uint8_t *end = ptr + length;
    uint8_t *code_ptr = dst++;
    uint8_t code = 0x01;
    while (ptr < end)
    {
        if (*ptr == 0) {
            FinishBlock(code);
        } else {
            *dst++ = *ptr;
            code++;
            if (code == 0xFF) {
                FinishBlock(code);
            }
        }
        ptr++;
    }
    FinishBlock(code);
}


/*
 * [_unstuff] decodes "length" bytes of
 * data at the location pointed to by "ptr",
 * writing the output to the location pointed
 * to by "dst".
 *** From http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
 */
void COBSPacket::_unstuff(const uint8_t *ptr, uint8_t length, uint8_t *dst) {
    const uint8_t *end = ptr + length;
    while (ptr < end) {
        int i, code = *ptr++;
        for (i = 1; i < code; i++) *dst++ = *ptr++;
        if (code < 0xFF) *dst++ = 0;
    }
}

void formatHex8(uint8_t n) {
    if (n < 16) {
        Serial.print("0" + String(n, HEX));
    } else {
        Serial.print(n, HEX);
    }
}

void formatHex16(uint16_t n) {
    formatHex8((n >> 8) & 0xff);
    formatHex8(n & 0xff);
}


/*
 *  Creates the packet and the 16-bit CRC and stuffs it into "buffer" using COBS
 */
void COBSPacket::create(const uint8_t *ptr, uint8_t length) {
    if (length > MAX_DATA_SIZE) {
        length = MAX_DATA_SIZE;
    }

    Serial.print("Data handed to packet: ");
    for (uint8_t i = 0; i < length; i++) {
        Serial.print(" ");
        formatHex8(ptr[i]);
    }
    Serial.println(" ");

    _packet.crc = _crc16(ptr, length);
    Serial.print("Create packet CRC: ");
    formatHex16(_packet.crc);
    Serial.println(" ");

    Serial.print("Copying bytes to packet: ");
    memcpy(_packet.data, ptr, length);
    Serial.println("OK");
    Serial.print("Data in packet: ");
    for (uint8_t i = 0; i < length; i++) {
        Serial.print(" ");
        formatHex8(_packet.data[i]);
    }
    Serial.println(" ");

    Serial.println("Stuffing packet data: ");
    Serial.println("  _packet_t size: " + String(sizeof(_packet_t), DEC));
    Serial.println("  _buffer size: " + String(sizeof(_buffer), DEC));
    _stuff((const uint8_t *)&_packet, sizeof(_packet_t), (uint8_t *)&_buffer);
    Serial.println("OK");

    _buffer[sizeof(_packet_t)] = PACKET_DELIMITER; // mark end of packet

    Serial.print("Data in packet:");
    for (uint8_t i = 0; i < MAX_FRAME_SIZE; i++) {
        Serial.print(" ");
        formatHex8(_buffer[i]);
    }
    Serial.println(" ");
}


/*
 *  Unstuffs the packet and the 16-bit CRC from "buffer" and copies to location "dst"
 */
void COBSPacket::copyData(uint8_t *dst, uint8_t length) {
    _unstuff(_buffer, sizeof(_packet_t), (uint8_t *)&_packet);
    Serial.print("Received CRC: ");
    formatHex16(_packet.crc);
    Serial.println(" ");
    uint16_t crc = _crc16(_packet.data, length);
    if (crc != _packet.crc) {
        Serial.println("CRC mismatch! Received data CRC = " + String(crc, HEX));
    } else {
        Serial.println("CRC MATCH! YAY!");
        memcpy(dst, _packet.data, length);
    }
}

void COBSPacket::dumpBuffer() {
    Serial.print("Buffer: ");
    for (uint8_t i = 0; (i < MAX_DATA_SIZE) && _buffer[i] != 0; i++) {
        Serial.print(_buffer[i], HEX);
        Serial.print(" ");
    }
    Serial.println("|");
}
