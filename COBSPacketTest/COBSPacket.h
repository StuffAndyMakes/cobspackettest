///
/// @file		COBSPacket.h
/// @brief		COBSPacket class library header
/// @details	Consistant Overhead Byte Stuffing library for Arduino/AVR
/// @n
/// @n @b		Project COBSPacketTest
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Andy
/// @author		StuffAndyMakes.com
///
/// @date		6/4/15 7:14 AM
/// @version	0.1
///
/// @copyright	(c) Andy, 2015
/// @copyright	MIT
///
/// @see		ReadMe.txt for references
///

/*
 *  I found the following articles and papers EXTREMELY helpful in this 
 *  reliable and working UART communications scheme:
 *
 *  Framing with COBS:
 *    http://www.embeddedrelated.com/showarticle/113.php
 *
 *  THE official COBS paper:
 *    http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
 *
 *  Painless Guide to CRC Error Detection Algorithms:
 *    http://www.ross.net/crc/download/crc_v3.txt
 *
 *  pyCRC - CRC calculator and C source code generator:
 *    https://github.com/tpircher/pycrc
 */

/**
 * Function for 16-bit CRC checks.
 *
 * Generated on Thu Jun  4 12:06:49 2015,
 * by pycrc v0.8, http://www.tty1.net/pycrc/
 * using the configuration:
 *    Width        = 16
 *    Poly         = 0x8005
 *    XorIn        = 0x0000
 *    ReflectIn    = True
 *    XorOut       = 0x0000
 *    ReflectOut   = True
 *    Algorithm    = table-driven
 *****************************************************************************/


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(ROBOTIS) // Robotis specific
#include "libpandora_types.h"
#include "pandora.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(RFDUINO) // RFduino specific
#include "Arduino.h"
#elif defined(SPARK) // Spark specific
#include "application.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

#ifndef COBSPacket_h
#define COBSPacket_h


#define PACKET_DELIMITER 0 // COBS is really that simple :D
#define MAX_FRAME_SIZE (128)
#define MAX_DATA_SIZE (127 - sizeof(uint16_t)) // frame size - CRC


typedef struct {
    uint16_t crc;
    uint8_t data[MAX_DATA_SIZE];
} _packet_t;


void formatHex8(uint8_t n);
void formatHex16(uint16_t n);


///
/// @class	encodes array of bytes (maybe from a struct) using COBS
///
class COBSPacket {

    _packet_t _packet;
    uint8_t _buffer[MAX_FRAME_SIZE];

public:

    /**
     * Update the crc value with new data.
     *
     * \param data     Pointer to a buffer of \a data_len bytes.
     * \param data_len Number of bytes in the \a data buffer.
     * \return         The computed crc value.
     *****************************************************************************/
    uint16_t _crc16(const uint8_t *data, size_t data_len);
    void _stuff(const uint8_t *ptr, uint8_t length, uint8_t *dst);
    void _unstuff(const uint8_t *ptr, uint8_t length, uint8_t *dst);
    
    void create(const uint8_t *ptr, uint8_t length);
    void copyData(uint8_t *dst, uint8_t length);
    void dumpBuffer();

};

#endif
