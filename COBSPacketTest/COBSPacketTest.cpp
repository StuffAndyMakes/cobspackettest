///
/// @mainpage	COBSPacketTest
///
/// @details	COBS Test on Arduino Mega 2560
/// @n
/// @n
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Andy
/// @author		StuffAndyMakes.com
/// @date		6/4/15 7:12 AM
/// @version	<#version#>
///
/// @copyright	(c) Andy, 2015
/// @copyright	CC = BY SA NC
///
/// @see		ReadMe.txt for references
///


///
/// @file		COBSPacketTest.cpp
/// @brief		Main sketch
///
/// @details	<#details#>
/// @n @a		Developed with [embedXcode+](http://embedXcode.weebly.com)
///
/// @author		Andy
/// @author		StuffAndyMakes.com
/// @date		6/4/15 7:12 AM
/// @version	<#version#>
///
/// @copyright	(c) Andy, 2015
/// @copyright	CC = BY SA NC
///
/// @see		ReadMe.txt for references
/// @n
///


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(ROBOTIS) // Robotis specific
#include "libpandora_types.h"
#include "pandora.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(RFDUINO) // RFduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "application.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

// Include application, user and local libraries
#include "COBSPacket.h"

// Prototypes

// Define variables and constants
COBSPacket p;

// Add setup code
void setup() {

    Serial.begin(115200);
    Serial.println("OK!");

//    uint8_t data[] = {4, 8, 0, 4, 4, 4, 8, 0, 3, 5};
    typedef struct {
        uint8_t system;
        uint8_t device;
        uint8_t command;
        uint8_t parameter;
        uint32_t serial;
    } data_t;
    data_t data;
    data.system = 'L';
    data.device = 'L';
    data.command = 'O';
    data.parameter = 0;
    data.serial = 1;

//    Serial.print("Before packet CRC: ");
//    uint16_t crc = p._crc16(data, sizeof(data));
//    formatHex16(crc);
//    Serial.println(" ");
//
//    Serial.println("Testing stuffing:");
//    uint8_t buffer[255];
//    p._stuff(data, 10, buffer);
//    Serial.print("  Stuffed data:");
//    for (uint8_t i = 0; buffer[i] != 0; i++) {
//        Serial.print(" ");
//        formatHex8(buffer[i]);
//    }
//    Serial.println(" ");
//
//    Serial.println("Testing unstuffing:");
//    uint8_t len = 0;
//    for (len = 0; buffer[len] != 0; len++);
//    Serial.println("  Stuffed data length: " + String(len, DEC));
//    p._unstuff(buffer, len, data);
//    Serial.print("  Unstuffed data:");
//    for (uint8_t i = 0; i < 10; i++) {
//        Serial.print(" ");
//        formatHex8(data[i]);
//    }
//    Serial.println(" ");

//    p.create(data, sizeof(data_t));
//    for (uint8_t i = 0; i < sizeof(data_t); i++) {
//        data[i] = 0xff;
//    }
//    p.copyData(data, sizeof(data_t));
//    Serial.println("Copy data back out of packet:");
//    for (uint8_t i = 0; i < sizeof(data_t); i++) {
//        Serial.print(data[i], DEC);
//        Serial.print(" ");
//    }
//    Serial.println(" ");
    
    p.create((uint8_t *)&data, sizeof(data_t));
    data.system = 0xff;
    data.device = 0xff;
    data.command = 0xff;
    data.parameter = 0xff;
    data.serial = 0xff;
    p.copyData((uint8_t *)&data, sizeof(data_t));
    Serial.println("Copy data back out of packet:");
    Serial.println("  System:    " + String(data.system, DEC));
    Serial.println("  Device:    " + String(data.device, DEC));
    Serial.println("  Command:   " + String(data.command, DEC));
    Serial.println("  Parameter: " + String(data.parameter, DEC));
    Serial.println("  Serial:    " + String(data.serial, DEC));
    
}

// Add loop code
void loop() {

}
