/**
 * \file crc16.c
 * Functions and types for CRC checks.
 *
 * Generated on Thu Jun  4 12:06:22 2015,
 * by pycrc v0.8, http://www.tty1.net/pycrc/
 * using the configuration:
 *    Width        = 16
 *    Poly         = 0x8005
 *    XorIn        = 0x0000
 *    ReflectIn    = True
 *    XorOut       = 0x0000
 *    ReflectOut   = True
 *    Algorithm    = table-driven
 *****************************************************************************/
#include "crc16.h"     /* include the header file generated with pycrc */
#include <stdlib.h>
#include <stdint.h>

/**
 * Static table used for the table_driven implementation.
 *****************************************************************************/
static const crc_t crc_table[16] = {
    0x0000, 0xcc01, 0xd801, 0x1400, 0xf001, 0x3c00, 0x2800, 0xe401,
    0xa001, 0x6c00, 0x7800, 0xb401, 0x5000, 0x9c01, 0x8801, 0x4400
};

/**
 * Reflect all bits of a \a data word of \a data_len bytes.
 *
 * \param data         The data word to be reflected.
 * \param data_len     The width of \a data expressed in number of bits.
 * \return             The reflected data.
 *****************************************************************************/
crc_t crc_reflect(crc_t data, size_t data_len)
{
    unsigned int i;
    crc_t ret;

    ret = data & 0x01;
    for (i = 1; i < data_len; i++) {
        data >>= 1;
        ret = (ret << 1) | (data & 0x01);
    }
    return ret;
}


/**
 * Update the crc value with new data.
 *
 * \param crc      The current crc value.
 * \param data     Pointer to a buffer of \a data_len bytes.
 * \param data_len Number of bytes in the \a data buffer.
 * \return         The updated crc value.
 *****************************************************************************/
crc_t crc_update(crc_t crc, const unsigned char *data, size_t data_len)
{
    unsigned int tbl_idx;

    while (data_len--) {
        tbl_idx = crc ^ (*data >> (0 * 4));
        crc = crc_table[tbl_idx & 0x0f] ^ (crc >> 4);
        tbl_idx = crc ^ (*data >> (1 * 4));
        crc = crc_table[tbl_idx & 0x0f] ^ (crc >> 4);

        data++;
    }
    return crc & 0xffff;
}





static char str[256] = "123456789";
static bool verbose = false;

void print_params(void);
static int get_config(int argc, char *argv[]);


static int get_config(int argc, char *argv[])
{
    int c;
    int option_index;
    static struct option long_options[] = {
        {"verbose",         0, 0, 'v'},
        {"check-string",    1, 0, 's'},
        {0, 0, 0, 0}
    };

    while (1) {
        option_index = 0;

        c = getopt_long (argc, argv, "w:p:n:i:u:o:s:vt", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
            case 0:
                printf ("option %s", long_options[option_index].name);
                if (optarg)
                    printf (" with arg %s", optarg);
                printf ("\n");
            case 's':
                memcpy(str, optarg, strlen(optarg) < sizeof(str) ? strlen(optarg) + 1 : sizeof(str));
                str[sizeof(str) - 1] = '\0';
                break;
            case 'v':
                verbose = true;
                break;
            case '?':
                return -1;
            case ':':
                fprintf(stderr, "missing argument to option %c\n", c);
                return -1;
            default:
                fprintf(stderr, "unhandled option %c\n", c);
                return -1;
        }
    }

    return 0;
}

void print_params(void)
{
    char format[20];

    snprintf(format, sizeof(format), "%%-16s = 0x%%0%dx\n", (unsigned int)(16 + 3) / 4);
    printf("%-16s = %d\n", "width", (unsigned int)16);
    printf(format, "poly", (unsigned int)0x8005);
    printf("%-16s = %s\n", "reflect_in", "true");
    printf(format, "xor_in", 0x0000);
    printf("%-16s = %s\n", "reflect_out", "true");
    printf(format, "xor_out", (unsigned int)0x0000);
    printf(format, "crc_mask", (unsigned int)0xffff);
    printf(format, "msb_mask", (unsigned int)0x8000);
}

/**
 * C main function.
 *
 * \return     0 on success, != 0 on error.
 *****************************************************************************/
int main(int argc, char *argv[])
{
    crc_t crc;

    get_config(argc, argv);
    crc = crc_init();
    crc = crc_update(crc, (unsigned char *)str, strlen(str));
    crc = crc_finalize(crc);

    if (verbose) {
        print_params();
    }
    printf("0x%lx\n", (unsigned long)crc);
    return 0;
}
