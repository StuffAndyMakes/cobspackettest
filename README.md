# README #

This is a research project to enable COBS byte stuffing for serial packets sent between microcontrollers, specifically the Arduino 8-bit (or AVR) flavors.

### How do I get set up? ###

This is an Arduino project, targeted to an Arduino Mega 2560 development board. I use embedXcode+ for my projects on AVR chips. I highly recommend it if you're on a Mac and you want a real, professional-grade IDE to do your work.

### More Infos ###

If there is more info to be found about this, it will be on http://StuffAndyMakes.com or http://OfficeChairiot.com. Feel free to contact me through those sites or shoot me a message here.